package az.edu.turing.module02.part02.lesson11.dao.impl;

import az.edu.turing.module02.part02.lesson11.dao.StudentDao;
import az.edu.turing.module02.part02.lesson11.dao.StudentEntity;
import netscape.javascript.JSObject;
//import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class StudentFileDao extends StudentDao {

    private static final String RESOURCE_PATH = "resource/";
    private static final String STUDENTS_FILE_PATH = RESOURCE_PATH.concat("students.txt");
//    private final ObjectMapper objectMapper;

    private String[] getFieldsArray(StudentEntity studentEntity) {
        String[] rtr = new String[5];
        rtr[0] = studentEntity.pin;
        rtr[1] = studentEntity.password;
        rtr[2] = "" + studentEntity.grade;
        rtr[3] = "" + studentEntity.age;
        rtr[4] = "" + studentEntity.studentId;

        return rtr;

    }


    @Override
    public StudentEntity save(StudentEntity studentEntity) {
        File file = new File(STUDENTS_FILE_PATH);
        try (FileWriter fw = new FileWriter(file, true)) {
            fw.write(String.format("\n%s", Arrays.toString(getFieldsArray(studentEntity))));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return studentEntity;
    }

    private StudentEntity parse(String stringOb) {
        StringBuilder[] arr = new StringBuilder[5];
        arr[0] = new StringBuilder();
        arr[1] = new StringBuilder();
        arr[2] = new StringBuilder();
        arr[3] = new StringBuilder();
        arr[4] = new StringBuilder();
        int i = 0;
        for (int j = 1; j < stringOb.length() - 1; j++) {
            String current = stringOb.substring(j, j + 1);
            String prev = stringOb.substring(j - 1, j);
            if ((prev + current).equals(", ")) {
                i++;
            } else if (!current.equals(",")) arr[i].append(current);
        }

        return new StudentEntity("" + arr[0],
                "" + arr[1],
                Double.parseDouble("" + arr[2]),
                Integer.parseInt("" + arr[3]),
                Integer.parseInt("" + arr[4]));
    }

    @Override
    public StudentEntity getById(int id) {
        File file = new File(STUDENTS_FILE_PATH);
        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {
            br.readLine();
            String line = "";
            while ((line = br.readLine()) != null) {
                StudentEntity parsed = parse(line);
                if (parsed.studentId == id) {
                    return parsed;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
